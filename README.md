# NewPipe to FreeTube

Bash script to convert NewPipe exported data to the data format FreeTube can import.

It converts:

- Subscriptions
- History
- Playlists

## Setup

Install the following dependencies:

- sqlite3
- jq

Arch:

`sudo pacman -S sqlite3 jq`

Debian/Ubuntu:

`sudo apt install sqlite3 jq`

Fedora:

`sudo dnf install sqlite3 jq`

## Step by Step

1. NewPipe: Settings -> Backup and restore -> Export database
2. Send the NewPipeData-*.zip to your computer
3. Place the nwpp2frtb.sh in the same directory
4. Open the terminal
5. Navigate to the directory yoou saved the file in
6. Run `./nwpp2frtb.sh`

If the command above does not work, run `chmod +x nwpp2frtb.sh` and then run the command again.

7. FreeTube: Settings -> Data Settings
8. Import the "freetube-..." files in the script directory