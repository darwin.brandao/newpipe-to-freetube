#!/bin/bash

unzip NewPipeData-*.zip;

# Subscriptions

echo '{
  "_id": "allChannels",
  "name": "All Channels",
  "bgColor": "#DD2C00",
  "textColor": "#FFFFFF",
  "subscriptions": []
}' | jq ".subscriptions = \
  $(sqlite3 newpipe.db -json \
    "SELECT 
      SUBSTR(url,33) as id, 
      name, 
      avatar_url as thumbnail 
    FROM subscriptions;"\
  )" | jq -c . > freetube-subscriptions.db;

# History

sqlite3 newpipe.db -json \
"SELECT 
  SUBSTR(s.url, 33) as videoId, 
  s.title, 
  s.uploader as author, 
  SUBSTR(s.uploader_url, 33) as authorId, 
  s.upload_date as published, 
  (SELECT '') as description, 
  s.view_count as viewCount, 
  s.duration as lengthSeconds, 
  s.duration as watchProgress,
  sh.access_date as timeWatched,
  (SELECT 'false') as isLive,
  (SELECT 'video') as type
FROM stream_history sh 
INNER JOIN streams s 
on sh.stream_id = s.uid;" | jq -c '.[]' > freetube-history.db

# Playlists

plCount=$( sqlite3 newpipe.db 'SELECT COUNT(*) FROM playlists' )

for ((i = 0; i < $plCount; i++)); do

  plId=$( sqlite3 newpipe.db  "SELECT uid FROM playlists LIMIT 1 OFFSET $i" )

  echo '{
    "playlistName": "",
    "protected": false,
    "description": "",
    "videos": [],
    "_id": "",
    "createdAt": 0,
    "lastUpdatedAt": 0
  }' | jq "
  .playlistName = \"$( sqlite3 newpipe.db "SELECT name FROM playlists WHERE uid = $plId" )\" |
  .videos = $( sqlite3 newpipe.db -json \
    "SELECT 
      SUBSTR(s.url, 33) as videoId, 
      s.title, 
      s.uploader as author, 
      SUBSTR(s.uploader_url, 33) as authorId, 
      s.duration as lengthSeconds, 
      (SELECT '$( date +%s )') as timeAdded, 
      (SELECT '$( uuidgen )') as playlistItemId, 
      (SELECT 'video') as type
    FROM streams s
    INNER JOIN playlist_stream_join ps
    ON s.uid = ps.stream_id
    WHERE ps.playlist_id = $plId" ) |
  ._id = \"ft-playlist--$( uuidgen )\" |
  .createdAt = $( date +%s ) |
  .lastUpdatedAt = $( date +%s )
  " | jq -c . >> freetube-playlists.db
done